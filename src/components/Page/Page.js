import React from 'react'
import Menu from 'Components/Menu/Menu'
import NavPage from 'Components/NavPage/NavPage'
import Banner from 'Components/Banner/Banner'
import Parse from 'parse'

const styleContent1 = {
  width: '100%',
}

const styleContent2 = {
  maxWidth: '65em',
  backgroundColor: '#f0f0f0',
  margin: '2em',
  marginTop: '3em',
  padding: '2em',
}

class Page extends React.Component {
  state = {
    index: 0,
    styleMenu: undefined,
    displayMenu: window.innerWidth < 1200 ? false : true
  }

  // hide or display menu
  menuDisplay = () => {
    const { displayMenu } = this.state
    if (!displayMenu) {
      this.setState({
        styleMenu: {
          transform: 'translate(-15em, 0)',
          minWidth: '0px',
          width: '0px'
        },
        marginLeft: '0em',
      })
    }
    else {
      this.setState({
        styleMenu: {
          transform: 'translate(0, 0)',
          minWidth: '15em',
          width: 'auto'
        },
        marginLeft: '15em',
      })
    }
  }

  // select child
  indexChange = (i) => {
    this.setState({ index: i || 0 })
  }

  handleDisplay = () => {
    const { displayMenu } = this.state
    this.setState({ displayMenu: !(displayMenu) })
  }

  componentDidUpdate(prevProps, prevState) {
    const { displayMenu } = this.state
    if (prevState.displayMenu !== displayMenu)
      this.menuDisplay()
  }

  componentDidMount() {
    const { history } = this.props
    if (!Parse.User.current())
      history.push("/login")
  }

  componentWillMount() {
    this.menuDisplay()
    window.addEventListener('resize', () => {
      this.setState({ displayMenu: window.innerWidth < 1200 ? false : true })
    })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => {
      this.setState({ displayMenu: window.innerWidth < 1200 ? false : true })
    })
  }

  render() {
    const { index, marginLeft, styleMenu } = this.state
    const { children, links, title } = this.props
    return (
      <div className="" >
        <div className="d-flex">
          <Menu styleProps={styleMenu} />
          <div className="block-center" style={Object.assign({}, styleContent1, { marginLeft })} >
            <Banner title={title} handleDisplay={this.handleDisplay}/>
            <div style={styleContent2} >
              <NavPage indexChange={this.indexChange} links={links} />
              {/* Pass f indexChange to children */}
              {React.cloneElement(children[index], { indexChange: this.indexChange })}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Page