import React from 'react'

const style = {
  marginBottom: '1em'
}

const styleLi = {
  minWidth: '10em'
}

const styleLink = {
  textAlign: 'center',
  color: 'silver',
  // margin: '0.2em',
  marginBottom: '0'
}

const renderLink = (links, indexChange) => {
  return links.map((link, index) =>
    <li key={`${index}-navPage`} style={styleLi} className="nav-item nav-page">
      <a style={styleLink} className="nav-link nav-page" onClick={() => indexChange(index)} href={`#/${link}`}>{link}</a>
    </li>
  )
}

const NavPage = ({ links, indexChange }) =>
  <ul style={style} className="nav nav-tabs justify-content-center">
    {links && renderLink(links, indexChange)}
  </ul>

export default NavPage