import React from 'react'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Editor from 'Elements/Editor/Editor'
import InputInline from 'Elements/InputInline/InputInline'
import SelectDropdown from 'Elements/SelectDropdown/SelectDropdown'
import { setData } from 'Utilis/ft'

class FormNew extends React.Component {
  state = {
    lang: '',
    type: '',
    techno: '',
    content: '',
    title: '',
    category: this.props.path,
    notify: (msg) => {
      toast.error(msg, {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: true
      })
    }
  }

  putValue = (key, value) => {
    this.setState({
      [key]: value
    })
  }

  clearState = () => {
    this.setState({
      lang: '',
      type: '',
      techno: '',
      content: undefined,
      title: '',
    })
  }

  render() {
    const { propsLang, propsType, propsTechno, getData } = this.props
    const { lang, type, techno, content, title } = this.state
    return (
      <div>
        <form onSubmit={(e) => setData(e, this.state, getData) && this.clearState()}>
          <div className="form-row">
            <div className="form-group col-8">
              <InputInline
                value={title}
                putValue={this.putValue}
              />
            </div>

            <div className="form-group col-1 offset-1">
              <button type="submit" className="btn btn-warning">Sauvegarder</button>
            </div>
          </div>

          <div className="form-row">
            <div className="form-group col-md-4">
              <SelectDropdown text='Langue'
                name="lang"
                value={lang && lang.get('name')}
                data={propsLang}
                putValueOnSelect={this.putValue}
              />
            </div>
            <div className="form-group col-md-4">
              <SelectDropdown text='Type'
                name="type"
                value={type && type.get('name')}
                data={propsType}
                putValueOnSelect={this.putValue}
              />
            </div>
            <div className="form-group col-md-4">
              <SelectDropdown text='Techno'
                name="techno"
                value={techno && techno.get('name')}
                data={propsTechno}
                putValueOnSelect={this.putValue}
              />
            </div>
            <div style={{ padding: 0 }} className="form-group col-md-12">
              <Editor
                putValueOnSelect={this.putValue}
                value={content}
              />
            </div>
          </div>

        </form>
        <div>
          <ToastContainer id="error" />
        </div>
      </div>
    )
  }
}

export default FormNew