import React from 'react'

import ReactTable from "react-table"
import "react-table/react-table.css"

const style = {
  fontWeight: '100',
}

const goTo = (e, f1, f2) => {
  f2(e)
  f1(1)
}

const buildColum = (arrayCol, readLabel, f1, f2) => {
  const col = arrayCol.map((e, i) => {
    return {
      Header: e[0],
      id: e[0],
      accessor: (x, index = i) => x && readLabel(x, i),
      maxWidth: e[1],
      headerStyle: Object.assign({}, e[2], { backgroundColor: '#ffffe6'}),
      style: Object.assign({}, e[2], {alignSelf: 'center'})
    }
  })
  const btn = {
    Header: <i className="fas fa-eye"></i>,
    id: "id",
    accessor: 'configUser',
    Cell: (e) => <button className='btn btn-warning' onClick={() => goTo(e.index, f1, f2)} >voir</button>,
    headerStyle: { backgroundColor: '#ffffe6'},
    width: 65
  }
  return col.push(btn) && col
}

const TableList = ({
  data,
  titleColum,
  readLabel,
  sorter,
  sizeLine,
  indexChange,
  updateView
}) => {
  return (
    <div>
      <ReactTable
        style={style}
        data={data}
        columns={buildColum(titleColum, readLabel, indexChange, updateView)}
        defaultSorted={[
          {
            id: { sorter },
            desc: true
          }
        ]}
        defaultPageSize={sizeLine}
        className="-striped -highlight"
      />
      <br />
    </div>
  )
}

export default TableList