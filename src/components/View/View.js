import React from 'react'

const style = {
  backgroundColor: 'white',
  padding: '1em',
  borderRadius: '3px'
}

class View extends React.Component {
  componentDidMount() {
    const { view } = this.props
    const e = document.getElementById('content')
    console.log(e)
    if (e)
      e.innerHTML = (view && view.get('content')) || '................................................'
  }

  render() {
    const { view } = this.props
    return (
      <div style={style}>
        <div>
          <button className='btn btn-warning ml-2 float-right' onClick={console.log} >Modifier</button>
          <button className='btn btn-warning mr-2 float-right' onClick={console.log} >Supprimer</button>
        </div>
        <h2>
          {(view && view.get('title')) || 'Aucune selection !'}
        </h2>
        <div id='content'>
          {/* {data put here} */}
        </div>
      </div>
    )
  }
}

export default View