import React from 'react'
import Parse from 'parse'

const style = {
  position: 'absolute',
  backgroundColor: '#2C2C2C',
  color: '#FFFFE2',
  right: '0',
  minWidth: '40%',
  marginTop: '0.7em',
  borderRadius: '40px 0 0 40px',
  fontSize: '20px'
}

const styleButton = {
  borderRadius: '40px',
  width: '5em',
  margin: '0.2em',
  backgroundColor: '#FFFFE2'
}

const styleLogout = {
  width: '2em',
  margin: '0.1em',
  backgroundColor: '#FFFFE2'
}

const styleTitle = {
  textAlign: 'right',
  lineHeight: '1.7em',
  marginRight: '2.5em',
  marginBottom: '0',
  flex: '1',
}

const logOut = () => {
  Parse.User.logOut()
    .then(() => window.location.reload())
    .catch(console.error)
}

const Banner = ({ title, handleDisplay }) => {
  return (
    <div className="d-flex" style={style}>
      <button
        style={styleButton}
        onClick={() => handleDisplay()}
      >
        Menu
      </button>
      <h3 className="" style={styleTitle}>
        {title}
      </h3>
      <button
        style={styleLogout}
        onClick={() => logOut()}
      >
        <i className="fas fa-power-off"></i>
      </button>
    </div>
  )
}

export default Banner