import React from 'react'
import LinkIcon from 'Elements/LinkIcon/LinkIcon'
import SideSepar from 'Elements/SideSepar/SideSepar'

const style = {
  position: 'fixed',
  backgroundImage: `url(Img/fond.png)`,
  height: '100vh',
  backgroundColor: '#ffffe6'
}

const styleBlocLink = {
  marginTop: '5em'
}

const Menu = ({ styleProps }) =>
  <div style={Object.assign({}, style, styleProps)} id='menu' className="block-menu">
    <div style={ styleBlocLink }>
      <div className='mt-4'>
        <ul className="nav flex-column">

          <LinkIcon path='/tutos' icon='fas fa-book'>Cours</LinkIcon>
          <LinkIcon path='/scripts' icon='fas fa-file-alt'>Scripts</LinkIcon>
          <LinkIcon path='/rss' icon='fas fa-rss-square'>RSS</LinkIcon>
          <SideSepar/>
          <LinkIcon path='/' icon='fas fa-user'>Me</LinkIcon>
          <SideSepar/>
          <LinkIcon path='/' icon='fas fa-cogs'>Configuration</LinkIcon>

        </ul>
      </div>
    </div>
  </div>

export default Menu