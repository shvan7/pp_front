import React from 'react'

const style = {
  height: '4em',
  zIndex: '1000',
  color: '#fff',
  backgroundColor: '#272727',
}

const styleUser = {
  marginLeft: '6em',
  padding: '0.4em'
}

const styleOff = {
  position: 'absolute',
  right: '0',
  backgroundColor: '#48bfdd',
  height: 'inherit'
}

const styleIcon = {
  fontSize: '40px',
  padding: '0.3em'
}

const InfoUser = ({ username }) =>
  <div className="ban-info d-inline-flex position-fixed" style={ style }>
    <div style={ styleUser }>
      <h2>Shvan7</h2>
    </div>
    <div style={ styleOff }>
      <i style={ styleIcon } className='fas fa-power-off'></i>
    </div>
  </div>

export default InfoUser