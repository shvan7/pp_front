import React from 'react'

import { BrowserRouter, Route } from 'react-router-dom'

import Login from 'Containers/Login/Login'
import PageTutos from 'Containers/PageTutos/PageTutos'
import PageScripts from 'Containers/PageScripts/PageScripts'
import PageRss from 'Containers/PageRss/PageRss' 

export default (
  <BrowserRouter>
    <div>
      <Route path="/login" component={Login} />
      <Route path="/(tutos|)/" component={PageTutos} />
      <Route path="/scripts" component={PageScripts} />
      <Route path="/rss" component={PageRss} />
    </div>
  </BrowserRouter>
)