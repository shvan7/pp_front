import {
  GET_TUTOS,
  GET_SCRIPTS,
  GET_TODOS,
  GET_RSS,
  GET_LANG,
  GET_TYPE,
  GET_TECHNO
} from "Redux/actionTypes"

const INITIAL_STATE = {
  tutos: [],
  scripts: [],
  todos: [],
  rss: [],
  lang: [],
  type: [],
  techno: []
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_TUTOS:
      return { ...state, tutos: action.process }
    case GET_SCRIPTS:
      return { ...state, scripts: action.process }
    case GET_TODOS:
      return { ...state, todos: action.process }
    case GET_RSS:
      return { ...state, rss: action.process }
    case GET_LANG:
      return { ...state, lang: action.process }
    case GET_TYPE:
      return { ...state, type: action.process }
    case GET_TECHNO:
      return { ...state, techno: action.process }
    default:
      return state
  }
}
