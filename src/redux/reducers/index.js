import { combineReducers } from 'redux'

import global from './global'
import dataStructure from './dataStructure'

const rootReducer = combineReducers({
  global,
  dataStructure
})

export default rootReducer