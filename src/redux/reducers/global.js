import {
  ERROR
} from "Redux/actionTypes"

const INITIAL_STATE = {
  error: null
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case ERROR:
      console.error(action.process)
      return { ...state, error: action.process }
    default:
      return state
  }
}
