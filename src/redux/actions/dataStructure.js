import {
  GET_TUTOS,
  GET_SCRIPTS,
  GET_TODOS,
  GET_RSS,
  GET_LANG,
  GET_TYPE,
  GET_TECHNO,
  ERROR
} from "Redux/actionTypes";
import { setDataToRedux } from 'Utilis/ft'

export function getData(data, category) {
  switch (data) {
    case "DataTutos":
      return category === '/tutos'
      ? setDataToRedux(data, GET_TUTOS, ERROR, category)
      : setDataToRedux(data, GET_SCRIPTS, ERROR, category)
    case "Todos":
      return setDataToRedux(data, GET_TODOS, ERROR)
    case "FluxRss":
      return setDataToRedux(data, GET_RSS, ERROR)
    case "Lang":
      return setDataToRedux(data, GET_LANG, ERROR)
    case "Type":
      return setDataToRedux(data, GET_TYPE, ERROR)
    case "Techno":
      return setDataToRedux(data, GET_TECHNO, ERROR)
    default:
      return setDataToRedux(data, "", ERROR)
  }
}