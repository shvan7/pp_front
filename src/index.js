import React from 'react'
import ReactDOM from 'react-dom'

// import { BrowserRouter } from 'react-router-dom'
import Parse from 'parse'

import config from 'react-global-configuration'
import configuration from 'Utilis/config'
import routes from './routes'

import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from 'Redux/reducers'
import thunk from 'redux-thunk'

import './index.css'
import * as serviceWorker from './serviceWorker'

config.set(configuration)
// Initialize Parse Server
Parse.initialize(config.get("PARSE.APP_ID"))
Parse.serverURL = `${config.get("PARSE.SERVER_URL")}${config.get("PARSE.SERVER_PARSE_MOUNT")}`
// Initialize Store (Redux) with thunk (for async) and devtools
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
const store = createStore(rootReducer,
  compose(
    applyMiddleware(thunk),
    composeEnhancers
  )
)

ReactDOM.render(
  <Provider store={store}>
    {routes}
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
