import React from 'react'

const style = {
  height: '1px',
  margin: '1em',
  backgroundColor: 'silver'
}

const SideSepar = () =>
  <div style={ style }></div>

export default SideSepar