import React from 'react'

const styleSpan = {
  backgroundColor: 'transparent',
  border: 'none',
  borderRadius: '0',
  borderBottom: '2px solid #ffffe6'
}

const styleInput = {
  backgroundColor: 'transparent',
  border: 'none',
  borderRadius: 0,
  borderBottom: '2px solid #ffffe6'
}

const InputInline = ({ value, putValue}) => {
  return (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <span style={styleSpan} className="input-group-text" id="basic-addon3">Titre :</span>
      </div>
      <input
        onChange={(e) => putValue('title', e.target.value)}
        style={styleInput}
        value={value}
        type="text"
        className="form-control"
      />
    </div>
  )
}

export default InputInline