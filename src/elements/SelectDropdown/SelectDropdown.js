import React from 'react'

const style= {
  borderColor: 'silver',
  backgroundColor: '#ffffe6',
  color: '#495057'
}

const renderData = (data, putValueOnSelect, name) => {
  return data && data.map((e, i) => {
    return (
      <button
        name={name}
        type='button'
        key={`item-dropdwn${i}`}
        className="dropdown-item"
        onClick={(f) => putValueOnSelect(f.target.name, e)}
      >
        {e.get('name').toUpperCase()}
      </button>
    )
  })
}

const SelectDropdown = ({
  text,
  data,
  value,
  putValueOnSelect,
  name
}) => {
  return (
    <div className="input-group mb-3 select-dropdown">
      <div className="input-group-prepend">
        <button style={ style } className="btn" data-toggle="dropdown" >
          {text}
        </button>
        <div className="dropdown-menu">
          {/* {choix des donnees du select} */}
          {renderData(data, putValueOnSelect, name)}
        </div>
      </div>
      {/* {doit prendre la valeur choissis} */}
      <input type="text" className="form-control" value={value.toUpperCase()} aria-label="Text input with dropdown button" disabled/>
    </div>
  )
}

export default SelectDropdown