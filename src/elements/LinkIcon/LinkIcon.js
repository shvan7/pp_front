import React from 'react'
import { Link } from "react-router-dom";

const style = {
  marginLeft: '1em',
  marginRight: '1em',
}

const LinkIcon = ({ path, children, icon }) =>
  <li className="nav-item">
    <Link className="nav-link menu-link" to={path}>
      <i style={style} className={icon}></i>
      {children}
    </Link>
  </li>

export default LinkIcon