import React from 'react'
import tinymce from 'tinymce/tinymce'
import 'tinymce/themes/modern/theme'

import 'tinymce/plugins/codesample'

import 'tinymce/plugins/advlist/plugin'
import 'tinymce/plugins/image'
import 'tinymce/plugins/link'
import 'tinymce/plugins/media'
import 'tinymce/plugins/textcolor'
import 'tinymce/plugins/code'

import 'tinymce/skins/lightgray/skin.min.css'
import './fr_FR'

class Editor extends React.Component {
    initTinymce() {
        const { putValueOnSelect } = this.props
        tinymce.init({
            selector: "textarea.tinymce",
            setup: (content) => {
                content.on("change", (e) => {
                    putValueOnSelect('content', e.level.content)
                })
                content.on("init", (e) => {
                    e.target.editorCommands.execCommand("fontName", false, "Geneva")
                })
            },
            mode: "textareas",
            branding: false,

            /* theme of the editor */
            theme: "modern",
            skin: false,
            content_css: false,
            codesample_content_css: './css/prism.css',

            /* width and height of the editor */
            width: "auto",
            height: 500,

            /* display statusbar */
            statubar: true,

            /* plugin */
            plugins: ["code", "codesample", "image", "textcolor", "media", "link"],
            codesample_dialog_width: 700,
            menubar: "",

            /* toolbar */
            toolbar: "codesample | insertfile undo redo | styleselect | bold italic | bullist numlist outdent indent | forecolor backcolor | fontsizeselect | link image | print preview media fullpage ",

            /* style */
            style_formats: [
                {
                    title: "Headers", items: [
                        { title: "Header 1", format: "h1" },
                        { title: "Header 2", format: "h2" },
                        { title: "Header 3", format: "h3" },
                        { title: "Header 4", format: "h4" },
                        { title: "Header 5", format: "h5" },
                        { title: "Header 6", format: "h6" }
                    ]
                },
                {
                    title: "Inline", items: [
                        { title: "Bold", icon: "bold", format: "bold" },
                        { title: "Italic", icon: "italic", format: "italic" },
                        { title: "Underline", icon: "underline", format: "underline" },
                        { title: "Strikethrough", icon: "strikethrough", format: "strikethrough" },
                        { title: "Superscript", icon: "superscript", format: "superscript" },
                        { title: "Subscript", icon: "subscript", format: "subscript" },
                        { title: "Code", icon: "code", format: "code" }
                    ]
                },
                {
                    title: "Blocks", items: [
                        { title: "Paragraph", format: "p" },
                        { title: "Blockquote", format: "blockquote" },
                        { title: "Div", format: "div" },
                        { title: "Pre", format: "pre" }
                    ]
                },
                {
                    title: "Alignment", items: [
                        { title: "Left", icon: "alignleft", format: "alignleft" },
                        { title: "Center", icon: "aligncenter", format: "aligncenter" },
                        { title: "Right", icon: "alignright", format: "alignright" },
                        { title: "Justify", icon: "alignjustify", format: "alignjustify" }
                    ]
                }
            ]

        })
    }

    componentDidMount() {
        this.initTinymce()
    }

    componentWillReceiveProps = (nextProps) => {
        const { value } = nextProps
        if (value === undefined) {
            tinymce.activeEditor.setContent('')
            tinymce.remove()
            this.initTinymce()
        }
    }

    render() {
        return (
            <div>
                <textarea className="tinymce" />
            </div>
        )
    }
}

export default Editor