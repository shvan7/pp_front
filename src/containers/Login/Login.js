import React from 'react'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import moment from 'moment'
import 'moment/locale/fr'
import Parse from 'parse'
import "./Login.scss"

const style = {
  backgroundImage: `url(Img/fond.png)`,
  height: window.innerHeight
}

class Login extends React.Component {
  state = {
    time: moment().format("HH:MM:ss"),
    date: moment().format("dddd D MMMM YYYY"),
    user: '',
    password: '',
  }

  notify = () => {
    toast.error("Incorrect username or password !", {
      position: "bottom-center",
      autoClose: 3000,
      hideProgressBar: true
    })
  }

  connect = (e) => {
    e.preventDefault()
    const { history } = this.props
    const { user, password } = this.state
    Parse.User.logIn(user, password)
      .then(() => {
        history.push("/tutos")
      })
      .catch(e => {
        this.setState({ password: "" })
        // console.error(e)
        this.notify()
      })
  }

  render() {
    const {
      user,
      password,
      date,
      time
    } = this.state
    return (
      <div style={style} className="page-login text-center pt-5">
        <h1>{time}</h1>
        <div>
          <h6>{date}</h6>
          <div>
            <form className="login" onSubmit={this.connect}>
              <p>
                <input type="text" name="pseudo" id="login" onChange={e => this.setState({ user: e.target.value })} value={user} placeholder="Name" autoComplete='on'/>
              </p>
              <p>
                <input type="password" name="pass" id="password" onChange={e => this.setState({ password: e.target.value })} value={password} placeholder="password" autoComplete='on'/>
              </p>
              <p className="login-submit mr-5">
                <button type="submit" className="login-button">Login</button>
              </p>

            </form>
          </div>
        </div>
        <div>
          <ToastContainer id="error" />
        </div>
      </div >
    )
  }
}

export default Login