import React from 'react';
import Page from 'Components/Page/Page'

class PageRss extends React.Component {
  render() {
    return (
      <Page title="RSS" links={['Liste', 'Vue', 'Nouveau']}>
        <h2>RSS</h2>
      </Page>
    )
  }
}

export default PageRss