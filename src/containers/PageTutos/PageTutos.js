import React from 'react'
import moment from 'moment'
import { connect } from 'react-redux'

import Page from 'Components/Page/Page'
import FormNew from 'Components/FormNew/FormNew'
import View from 'Components/View/View'
import TableList from 'Components/TableList/TableList'

import { getData } from 'Redux/actions'

class PageTutos extends React.Component {
  state = {
    view: undefined
  }

  componentWillMount() {
    const { getData } = this.props
    getData('DataTutos', '/tutos')
    getData('Lang')
    getData('Type')
    getData('Techno')
  }

  updateView = (index) => {
    const { tutos } = this.props
    this.setState({ view: tutos[index]})
  }

  configColumns = () => {
    return [
      ['Auteur', 90, {
        textAlign: 'center'
      }],
      ['Titre', 200, {
        paddingLeft: '1em'
      }],
      ['Informations', 400, {
      }],
      ['Crée le ..', 160, {
        textAlign: 'center'
      }],
      ['Modifié le ..', 160, {
        textAlign: 'center'
      }]
    ]
  }

  readDataLabel = (e, index) => {
    const accessor = [
      e.get("p_autor") && e.get("p_autor").get('username'),
      e.get("title"),
      `
        ${(e.get('p_lang') && e.get('p_lang').get('name').toUpperCase()) || ''}
        ${(e.get('p_type') && '- ' + e.get('p_type').get('name').toUpperCase()) || ''}
        ${(e.get('p_techno') && '- ' + e.get('p_techno').get('name').toUpperCase()) || ''}
      `,
      moment(e.createdAt).format('DD/MM/YYYY à HH:mm'),
      moment(e.updatedAt).format('DD/MM/YYYY à HH:mm')
    ]
    return accessor[index]
  }

  render() {
    const { tutos, lang, type, techno, getData, history } = this.props
    const { view } = this.state
    return (
      <Page {...this.props} title="Tutos" links={['Liste', 'Vue', 'Nouveau']}>

        <TableList
          data={tutos}
          titleColum={this.configColumns()}
          readLabel={this.readDataLabel}
          sorter='Crée le ..'
          sizeLine={20}
          updateView={this.updateView}
        />

        <View view={view} />

        <FormNew
          propsLang={lang}
          propsType={type}
          propsTechno={techno}
          getData={getData}
          path={history.location.pathname}
        />

      </Page>
    )
  }
}

const mapStateToProps = ({ dataStructure }) => {
  const { tutos, lang, type, techno } = dataStructure
  return { tutos, lang, type, techno }
}

export default connect(mapStateToProps, {
  getData
})(PageTutos)