import Parse from 'parse'

export const setData = (e, data, getData) => {
  e.preventDefault()
  const {
    content,
    title,
    lang,
    type,
    techno,
    notify,
    category
  } = data
  const params = {
    idLang: typeof lang === 'object' && lang.id,
    idType: typeof type === 'object' && type.id,
    idTechno: typeof techno === 'object' && techno.id,
    content,
    title,
    category
  }
  return Parse.Cloud.run("createDataTutos", params)
    .then(() => getData('DataTutos', category))
    .catch((msg) => {
      const regex = /\[(.*?)\]/
      const newMsg = msg.message.match(regex)
      notify(newMsg && newMsg[1])
    })
}