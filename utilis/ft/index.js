import {
  setDataToRedux,
} from './getData'

import {
  setData
} from './setData'

export {
  setDataToRedux,
  setData
}