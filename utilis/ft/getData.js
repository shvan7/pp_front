import Parse from 'parse'

// Get data by className
export const setDataToRedux = (className, type, typeError, category) => {
  return dispatch => {
    const query = new Parse.Query(className)
    // for DataTutos
    if (className === 'DataTutos') {
      query.equalTo('category', category)
      query.include('p_lang')
      query.include('p_autor')
      query.include('p_type')
      query.include('p_techno')
    }
    query.find()
      .then(res => dispatch({ type, process: res }))
      .catch(err => dispatch({ type: typeError, process: err }))
  }
}

// Get data relation
export const getDataPointer = (idPointer, className) => {
  const query = new Parse.Query(className)
  query.equalTo('objectId', idPointer)
  return Promise.resolve(query.first())
}

// Get data role by user
export const setDataRoleByUser = (tabUser, type, typeError) => {
  return dispatch => {
    const tab = tabUser.map(parseObject => {
      const query = new Parse.Query('_Role')
      query.equalTo('users', parseObject)
      return query.first()
    })
    Promise.all(tab)
      .then(res => dispatch({ type, payload: res }))
      .catch(err => dispatch({ type: typeError, payload: err }))
  }
}